import 'dart:math';
import '../../domain/event/event.dart';

final mockAllEvent = Event(
    "1",
    "Have fun",
    "https://storage.yandexcloud.net/party-swipe-bucket/pi_team.32.jpg",
    "Saint-Petersburg",
    DateTime(2023, 11, 11, 11, 11, 11),
    "We need to have some fun",
    List<String>.of(["Coding", "Fun"]), 123);

final mockMyEvent = Event(
    "1",
    "That's mine",
    "https://storage.yandexcloud.net/party-swipe-bucket/pi_team.32.jpg",
    "Saint-Petersburg",
    DateTime(2023, 11, 11, 11, 11, 11),
    "Yes, i did it",
    List<String>.of(["Coding", "Fun"]), 123);


final mockAllEventList = List<Event>.filled(10, mockAllEvent);
final mockMyEventList = List<Event>.filled(10, mockMyEvent);

Future<List<Event>> getMockAllEventList(int pageKey, int pageLen) async {
  if (pageKey >= mockAllEventList.length) return List.empty();
  return mockAllEventList.sublist(pageKey, max(pageKey + pageLen, mockAllEventList.length));
}

Future<List<Event>> getMockMyEventList(int pageKey, int pageLen) async {
  if (pageKey >= mockMyEventList.length) return List.empty();
  return mockMyEventList.sublist(pageKey, max(pageKey + pageLen, mockMyEventList.length));
}