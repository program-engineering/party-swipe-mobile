import 'package:flutter_bloc/flutter_bloc.dart';
import '../auth/auth_cubit.dart';
import 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  final AuthCubit authCubit;
  ProfileCubit(this.authCubit, ProfileState state) : super(state);

  void signOut() {
    emit(ProfileState.loading);
    authCubit.signOut();
    emit(ProfileState.signInScreen);
  }

  void goToSignInScreen() {
    emit(ProfileState.signInScreen);
  }

  void goToProfileScreen() {
    emit(ProfileState.profileScreen);
  }

  void goToHomeScreen() {
    emit(ProfileState.homeScreen);
  }

  void goToMyEventsScreen() {
    emit(ProfileState.myEventsScreen);
  }

  String? getUsername() {
    return authCubit.authRepository.getUsername();
  }

  String? getEmail()  {
    return authCubit.authRepository.getEmail();
  }
}
