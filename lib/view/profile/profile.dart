import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:firebase_auth/firebase_auth.dart' hide User;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../data/all_categories/all_categories.dart';
import '../../domain/auth/auth_repository.dart';
import '../auth/sign_in.dart';
import '../home/home.dart';
import '../my_events/my_events.dart';
import '../auth/auth_cubit.dart';
import '../auth/auth_state.dart';
import '../navigation/navigation_bar.dart';
import '../../data/mock_users/mock_users.dart';
import '../../domain/user/user.dart';
import '../widgets/category_list.dart';
import 'profile_state.dart';
import 'profile_cubit.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final User _currentUser = mockUser;
  final _bioController = TextEditingController(text: mockUser.bio);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProfileCubit(
          AuthCubit(AuthRepository(FirebaseAuth.instance), AuthState.signInScreen), ProfileState.profileScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          bottomNavigationBar: SizedBox(
            height: bottomNavigationBarHeight,
            child: CustomBottomNavigationBar(context, 1),
          ),
          body: BlocListener<ProfileCubit, ProfileState>(
            listener: (context, state) {
              if (state == ProfileState.homeScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
              } else if (state == ProfileState.signInScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const SignInPage()));
              } else if (state == ProfileState.myEventsScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const MyEventsPage()));
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - bottomNavigationBarHeight,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 32, 16, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Row(
                        children: [
                          _Avatar(),
                          SizedBox(width: 8),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                _UsernameText(),
                                SizedBox(height: 16),
                                _EmailText(),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      _Bio(bioController: _bioController, currentUser: _currentUser),
                      const SizedBox(height: 8),
                      const _MyEventsButton(),
                      const SizedBox(height: 16),
                      const _InterestsText(),
                      const SizedBox(height: 16),
                      _InterestsSelection(currentUser: _currentUser),
                      const Spacer(),
                      const _SignOutButton(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _AvatarImage extends StatelessWidget {
  const _AvatarImage();

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: 130,
        maxHeight: 130,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(16),
          border: Border.all(width: 1),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: CachedNetworkImage(
            imageUrl: "https://storage.yandexcloud.net/party-swipe-bucket/pi_team.32.jpg",
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
      ),
    );
  }
}

class _SelectImageButton extends StatelessWidget {
  const _SelectImageButton();

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        maxWidth: 30,
        maxHeight: 30,
      ),
      child: IconButton(
        onPressed: () async {
          final picker = ImagePicker();
          final pickedFile = await picker.pickImage(source: ImageSource.gallery);
        },
        icon: const Icon(
          Icons.edit,
          size: 15,
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(Theme.of(context).colorScheme.secondaryContainer),
          foregroundColor: MaterialStatePropertyAll(Theme.of(context).colorScheme.onSecondaryContainer),
        ),
      ),
    );
  }
}

class _Avatar extends StatelessWidget {
  const _Avatar();

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: 140,
        maxHeight: 140,
      ),
      child: Stack(
        children: [
          Container(
            alignment: Alignment.topLeft,
            child: const _AvatarImage(),
          ),
          Container(
            alignment: Alignment.bottomRight,
            child: const _SelectImageButton(),
          ),
        ],
      ),
    );
  }
}

class _UsernameText extends StatelessWidget {
  const _UsernameText();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(
              context.read<ProfileCubit>().getUsername() ?? "",
              style: const TextStyle(
                fontSize: 24,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _EmailText extends StatelessWidget {
  const _EmailText();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(
              context.read<ProfileCubit>().getEmail() ?? "",
              style: const TextStyle(fontSize: 18, color: Colors.black45),
            ),
          ),
        ),
      ],
    );
  }
}

class _BioText extends StatelessWidget {
  const _BioText({
    required TextEditingController bioController,
    required User currentUser,
  })  : _bioController = bioController,
        _currentUser = currentUser;

  final TextEditingController _bioController;
  final User _currentUser;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            keyboardType: TextInputType.multiline,
            minLines: 1,
            maxLines: 6,
            maxLength: 300,
            controller: _bioController,
            onSubmitted: (text) => (_currentUser.bio = text),
            readOnly: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(16),
              ),
              hintText: "bio".tr(),
              counterText: "",
            ),
          ),
        ),
      ],
    );
  }
}

class _EditBioButton extends StatelessWidget {
  const _EditBioButton({
    required TextEditingController bioController,
    required User currentUser,
  })  : _bioController = bioController,
        _currentUser = currentUser;

  final TextEditingController _bioController;
  final User _currentUser;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        maxHeight: 30,
        maxWidth: 30,
      ),
      child: IconButton(
        onPressed: () => (showDialog(
            context: context,
            builder: (BuildContext context) {
              return _EditBioDialog(bioController: _bioController, currentUser: _currentUser);
            })),
        icon: const Icon(
          Icons.edit,
          size: 15,
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStatePropertyAll(Theme.of(context).colorScheme.secondaryContainer),
          foregroundColor: MaterialStatePropertyAll(Theme.of(context).colorScheme.onSecondaryContainer),
        ),
      ),
    );
  }
}

class _Bio extends StatelessWidget {
  const _Bio({
    required TextEditingController bioController,
    required User currentUser,
  })  : _bioController = bioController,
        _currentUser = currentUser;

  final TextEditingController _bioController;
  final User _currentUser;

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Stack(
        children: [
          Container(
            alignment: Alignment.topLeft,
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
            child: _BioText(bioController: _bioController, currentUser: _currentUser),
          ),
          Container(
            alignment: Alignment.bottomRight,
            child: _EditBioButton(bioController: _bioController, currentUser: _currentUser),
          ),
        ],
      ),
    );
  }
}

class _EditBioDialog extends StatelessWidget {
  const _EditBioDialog({
    required TextEditingController bioController,
    required User currentUser,
  })  : _bioController = bioController,
        _currentUser = currentUser;

  final TextEditingController _bioController;
  final User _currentUser;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: TextField(
        keyboardType: TextInputType.multiline,
        minLines: 1,
        maxLines: 6,
        maxLength: 300,
        controller: _bioController,
        onSubmitted: (text) {
          _currentUser.bio = text;
          Navigator.pop(context);
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(16),
          ),
          hintText: "bio".tr(),
        ),
      ),
    );
  }
}

class _MyEventsButton extends StatelessWidget {
  const _MyEventsButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: OutlinedButton(
            onPressed: () {
              if (context.read<ProfileCubit>().state == ProfileState.loading) return;
              context.read<ProfileCubit>().goToMyEventsScreen();
            },
            style: const ButtonStyle(
              foregroundColor: MaterialStatePropertyAll(Colors.black),
            ),
            child: Row(
              children: [
                Icon(MdiIcons.heartOutline),
                const SizedBox(width: 16),
                Text("my_events".tr()),
                const Spacer(),
                const Icon(Icons.keyboard_arrow_right_sharp),
              ],
            ),
          ),
        )
      ],
    );
  }
}

class _InterestsText extends StatelessWidget {
  const _InterestsText();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "interests".tr(),
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        Text(
          "specify_interests".tr(),
          style: const TextStyle(color: Colors.black45),
        )
      ],
    );
  }
}

class _InterestsSelection extends StatefulWidget {
  const _InterestsSelection({
    required User currentUser,
  }) : _currentUser = currentUser;

  final User _currentUser;

  @override
  State<_InterestsSelection> createState() => _InterestsSelectionState();
}

class _InterestsSelectionState extends State<_InterestsSelection> {
  late TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Autocomplete<String>(
          onSelected: (text) {
            setState(() {
              if (!widget._currentUser.categories.contains(text)) {
                widget._currentUser.categories.add(text);
              } else {
                widget._currentUser.categories.remove(text);
              }
            });
            textEditingController.text = "";
            // FocusManager.instance.primaryFocus?.unfocus();
          },
          optionsBuilder: (TextEditingValue textEditingValue) {
            return allCategories
                .where((String option) => option.toLowerCase().contains(textEditingValue.text.toLowerCase()));
          },
          optionsViewBuilder: (context, onSelected, options) {
            return Align(
              alignment: Alignment.topLeft,
              child: Material(
                borderRadius: BorderRadius.circular(16),
                color: Theme.of(context).colorScheme.secondaryContainer,
                child: SizedBox(
                  height: 200.0,
                  width: MediaQuery.of(context).size.width - 32,
                  child: ListView.separated(
                    padding: EdgeInsets.zero,
                    itemCount: options.length,
                    separatorBuilder: (context, index) {
                      return Container(height: 1, color: Colors.black);
                    },
                    itemBuilder: (BuildContext context, int index) {
                      final option = options.elementAt(index);
                      return InkWell(
                        onTap: () {
                          onSelected(option);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 16,
                                height: 16,
                                child: Checkbox(
                                  value: widget._currentUser.categories.contains(option),
                                  onChanged: (value) => onSelected(option),
                                ),
                              ),
                              const SizedBox(width: 16),
                              Text(
                                option,
                                style: TextStyle(color: Theme.of(context).colorScheme.onSecondaryContainer),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            );
          },
          fieldViewBuilder: (BuildContext context, TextEditingController fieldTextEditingController,
              FocusNode fieldFocusNode, VoidCallback onFieldSubmitted) {
            textEditingController = fieldTextEditingController;
            return TextField(
              controller: fieldTextEditingController,
              focusNode: fieldFocusNode,
              onSubmitted: (text) {
                fieldTextEditingController.text = "";
                FocusManager.instance.primaryFocus?.unfocus();
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                labelText: "choose_categories".tr(),
              ),
            );
          },
        ),
        const SizedBox(height: 16),
        CategoryList(categories: widget._currentUser.categories),
      ],
    );
  }
}

class _SignOutButton extends StatelessWidget {
  const _SignOutButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TextButton(
          onPressed: () {
            if (context.read<ProfileCubit>().state == ProfileState.loading) return;
            FocusManager.instance.primaryFocus?.unfocus();
            context.read<ProfileCubit>().signOut();
          },
          style: TextButton.styleFrom(
            foregroundColor: Colors.red,
          ),
          child: Text("log_out".tr()),
        ),
      ],
    );
  }
}
