enum ProfileState {
  profileScreen(""),
  homeScreen(""),
  signInScreen(""),
  myEventsScreen(""),
  loading(""),
  unknownException("unknown");

  const ProfileState(this.message);

  final String message; // contain info about the state or error code in case state is error
}
