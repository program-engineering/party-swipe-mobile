enum HomeState {
  profileScreen(""),
  allEventsScreen(""),
  homeScreen(""),
  loading(""),
  unknownException("unknown");

  const HomeState(this.message);

  final String message; // contain info about the state or error code in case state is error
}
