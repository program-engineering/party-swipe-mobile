import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../navigation/navigation_bar.dart';
import '../profile/profile.dart';
import '../all_events/all_events.dart';
import 'home_state.dart';
import 'home_cubit.dart';
import '../widgets/logo_image.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeCubit(HomeState.homeScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          bottomNavigationBar: SizedBox(
            height: bottomNavigationBarHeight,
            child: CustomBottomNavigationBar(context, 0),
          ),
          body: BlocListener<HomeCubit, HomeState>(
            listener: (context, state) {
              if (state == HomeState.profileScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const ProfilePage()));
              } else if (state == HomeState.allEventsScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const AllEventsPage()));
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - bottomNavigationBarHeight,
                width: MediaQuery.of(context).size.width,
                child: const Padding(
                  padding: EdgeInsets.fromLTRB(16, 32, 16, 0),
                  child: Column(
                    children: [
                      Spacer(),
                      LogoImage(),
                      Spacer(),
                      SizedBox(height: 16),
                      Text("Home page (WIP)"),
                      // SizedBox(height: 16),
                      // _ProfileButton(),
                      // SizedBox(height: 16),
                      // _AllEventsButton(),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),

      ),
    );
  }
}
