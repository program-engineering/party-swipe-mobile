import 'package:flutter_bloc/flutter_bloc.dart';
import 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit(HomeState state) : super(state);

  void goToProfileScreen() {
    emit(HomeState.profileScreen);
  }

  void goToAllEventsScreen() {
    emit(HomeState.allEventsScreen);
  }
}
