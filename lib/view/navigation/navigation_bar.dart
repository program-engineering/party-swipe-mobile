import 'package:flutter/material.dart';
import '../all_events/all_events.dart';
import '../home/home.dart';
import '../profile/profile.dart';

const double bottomNavigationBarHeight = 60;

class CustomBottomNavigationBar extends BottomNavigationBar {
  final BuildContext context;
  int selectedIndex;

  CustomBottomNavigationBar(this.context, this.selectedIndex, {super.key}) : super(
    onTap: (value) {
      if (value == selectedIndex) return;
      if (value == 0) {
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
      }
      if (value == 1) {
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => const ProfilePage()));
      }
      if (value == 2) {
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (context) => const AllEventsPage()));
      }
    },
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Home',
        backgroundColor: Colors.deepPurple,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.account_circle),
        label: 'Profile',
        backgroundColor: Colors.deepPurple,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.event),
        label: 'Events',
        backgroundColor: Colors.deepPurple,
      ),
    ],
    currentIndex: selectedIndex,
    showUnselectedLabels: true,
    type: BottomNavigationBarType.fixed,
    backgroundColor: Theme.of(context).colorScheme.primary,
    unselectedItemColor: Theme.of(context).colorScheme.onPrimary,
    selectedItemColor: Theme.of(context).colorScheme.tertiaryContainer,
  );
}
