import 'package:flutter_bloc/flutter_bloc.dart';
import 'event_card_state.dart';

class EventCardCubit extends Cubit<EventCardState> {
  EventCardCubit(EventCardState state) : super(state);
}
