enum EventCardState {
  normal(""),
  loading(""),
  unknownException("unknown");

  const EventCardState(this.message);

  final String message;
}
