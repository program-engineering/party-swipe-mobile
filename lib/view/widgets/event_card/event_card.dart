import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../../../domain/event/event.dart';
import '../../widgets/category_list.dart';
import 'event_card_cubit.dart';
import 'event_card_state.dart';

class EventCard extends StatelessWidget {
  const EventCard({
    super.key,
    required this.event,
    required this.hasLikes,
  });

  final Event event;
  final bool hasLikes;

  @override
  Widget build(BuildContext context) {
    Widget icon = (hasLikes)
        ? Row(
            children: [
              const SizedBox(width: 16),
              Icon(
                MdiIcons.heartOutline,
                color: Colors.red,
              ),
              const SizedBox(width: 4),
              Text(event.likes.toString()),
            ],
          )
        : const SizedBox.shrink();

    return BlocProvider(
      create: (context) => EventCardCubit(EventCardState.normal),
      child: BlocBuilder<EventCardCubit, EventCardState>(builder: (context, state) {
        return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
            ),
            child: Card(
              color: Theme.of(context).colorScheme.onSecondary,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  children: [
                    Row(
                      children: [
                        _Picture(event: event),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Center(
                            child: Text(
                              event.name,
                              style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                                    color: Theme.of(context).colorScheme.primary,
                                  ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Text(
                      event.description,
                      style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        const SizedBox(width: 32),
                        Icon(
                          Icons.location_on,
                          color: Theme.of(context).colorScheme.primary,
                        ),
                        const SizedBox(width: 16),
                        Text(
                          event.location,
                          style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                        ),
                      ],
                    ),
                    const SizedBox(height: 16),
                    Row(
                      children: [
                        const SizedBox(width: 32),
                        Icon(
                          MdiIcons.clock,
                          color: Theme.of(context).colorScheme.primary,
                        ),
                        const SizedBox(width: 16),
                        Text(
                          DateFormat("dd.mm.yyyy, hh:mm").format(event.datetime),
                          style: TextStyle(color: Theme.of(context).colorScheme.secondary),
                        ),
                      ],
                    ),
                    const SizedBox(height: 32),
                    Row(
                      children: [
                        Expanded(
                          child: CategoryList(
                            categories: event.categories,
                          ),
                        ),
                        const SizedBox(width: 16),
                        icon,
                      ],
                    ),
                  ],
                ),
              ),
            ));
      }),
    );
  }
}

class _Picture extends StatelessWidget {
  const _Picture({
    required this.event,
  });

  final Event event;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(
        maxWidth: 130,
        maxHeight: 130,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(16),
          border: Border.all(width: 1),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: CachedNetworkImage(
            imageUrl: event.pictureUrl,
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
      ),
    );
  }
}
