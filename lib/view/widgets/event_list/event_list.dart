import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../domain/event/event.dart';
import '../event_card/event_card.dart';
import 'event_list_cubit.dart';
import 'event_list_state.dart';

class EventList extends StatefulWidget {
  const EventList({
    super.key,
    required this.getEventList,
    required this.hasLikes,
  });

  final Future<List<Event>> Function(int pageKey, int pageLen) getEventList;
  final bool hasLikes;

  @override
  State<EventList> createState() => _EventListViewState();
}

class _EventListViewState extends State<EventList> {
  static const _pageSize = 3;
  final PagingController<int, Event> _pagingController = PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await widget.getEventList(pageKey, _pageSize);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => EventListCubit(EventListState.normal),
        child: BlocBuilder<EventListCubit, EventListState>(builder: (context, state) {
          return PagedListView<int, Event>(
            pagingController: _pagingController,
            builderDelegate: PagedChildBuilderDelegate<Event>(
              itemBuilder: (context, item, index) => EventCard(event: item, hasLikes: widget.hasLikes,),
            ),
          );
        }));
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
