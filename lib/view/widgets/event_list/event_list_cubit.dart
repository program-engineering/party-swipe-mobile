import 'package:flutter_bloc/flutter_bloc.dart';
import 'event_list_state.dart';

class EventListCubit extends Cubit<EventListState> {
  EventListCubit(EventListState state) : super(state);
}
