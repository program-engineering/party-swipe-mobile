enum EventListState {
  normal(""),
  loading(""),
  unknownException("unknown");

  const EventListState(this.message);

  final String message;
}
