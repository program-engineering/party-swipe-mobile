import 'package:flutter_bloc/flutter_bloc.dart';

import 'my_events.dart';
import 'my_events_state.dart';

class MyEventsCubit extends Cubit<MyEventsState> {
  MyEventsCubit(MyEventsState state) : super(state);

  void goToProfileScreen() {
    emit(MyEventsState.profileScreen);
  }

  void goToMyEventsScreen() {
    emit(MyEventsState.myEventsScreen);
  }

  void goToAddEventScreen() {
    emit(MyEventsState.addEventScreen);
  }
}
