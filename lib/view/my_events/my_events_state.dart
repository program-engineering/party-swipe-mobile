enum MyEventsState {
  profileScreen(""),
  myEventsScreen(""),
  addEventScreen(""),
  loading(""),
  unknownException("unknown");

  const MyEventsState(this.message);

  final String message;
}
