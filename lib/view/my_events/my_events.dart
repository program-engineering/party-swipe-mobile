import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../profile/profile.dart';
import '../add_event/add_event.dart';
import '../widgets/event_list/event_list.dart';
import '../../data/mock_events/mock_events.dart';
import 'my_events_cubit.dart';
import 'my_events_state.dart';

class MyEventsPage extends StatefulWidget {
  const MyEventsPage({super.key});

  @override
  State<MyEventsPage> createState() => _MyEventsPageState();
}

class _MyEventsPageState extends State<MyEventsPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MyEventsCubit(MyEventsState.myEventsScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: BlocListener<MyEventsCubit, MyEventsState>(
            listener: (context, state) {
              if (state == MyEventsState.profileScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const ProfilePage()));
              } else if (state == MyEventsState.addEventScreen) {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const AddEventScreen()));
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          _ProfileButton(),
                          SizedBox(width: 16),
                          Expanded(child: _AddEventScreen()),
                        ],
                      ),
                      SizedBox(height: 16),
                      Flexible(
                        child: EventList(
                          getEventList: getMockMyEventList,
                          hasLikes: true,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _ProfileButton extends StatelessWidget {
  const _ProfileButton();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        if (context.read<MyEventsCubit>().state == MyEventsState.loading) return;
        context.read<MyEventsCubit>().goToProfileScreen();
      },
      icon: const Icon(Icons.keyboard_arrow_left_sharp),
    );
  }
}

class _AddEventScreen extends StatelessWidget {
  const _AddEventScreen();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<MyEventsCubit>().state == MyEventsState.loading) return;
              context.read<MyEventsCubit>().goToAddEventScreen();
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("add_event".tr()),
          ),
        ),
      ],
    );
  }
}
