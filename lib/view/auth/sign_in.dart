import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import '../../domain/auth/auth_repository.dart';
import 'auth_cubit.dart';
import 'auth_state.dart';
import 'sign_up.dart';
import 'email_verification.dart';
import 'password_reset.dart';
import '../home/home.dart';
import '../widgets/logo_image.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  var _exceptionMessage = "";

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthCubit(AuthRepository(FirebaseAuth.instance), AuthState.success),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: BlocListener<AuthCubit, AuthState>(
            listener: (context, state) {
              if (state == AuthState.signUpScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const SignUpPage()));
              } else if (state == AuthState.emailVerificationScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const EmailVerificationPage()));
              } else if (state == AuthState.passwordResetScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const PasswordResetPage()));
              } else if (state == AuthState.success) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
              } else if (state.isException) {
                setState(() {
                  _exceptionMessage = state.message.tr();
                });
              }
              if (!state.isException) {
                _exceptionMessage = "";
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                  child: Column(
                    children: [
                      const SizedBox(height: 32),
                      const LogoImage(),
                      const SizedBox(height: 32),
                      _EmailTextField(emailController: _emailController),
                      const SizedBox(height: 16),
                      _PasswordTextField(passwordController: _passwordController),
                      _ErrorText(errorMessage: _exceptionMessage), // 16 offset
                      const _ForgotPasswordButton(),
                      const SizedBox(height: 16),
                      _SignInButton(emailController: _emailController, passwordController: _passwordController),
                      const SizedBox(height: 16),
                      Text("or_sign_in_with".tr()),
                      const SizedBox(height: 16),
                      const _SignInWithGoogleButton(),
                      const Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("no_account".tr()),
                          const _GoToSignUp(),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _ForgotPasswordButton extends StatelessWidget {
  const _ForgotPasswordButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        TextButton(
          onPressed: () {
            if (context.read<AuthCubit>().state == AuthState.loading) return;
            context.read<AuthCubit>().goToPasswordResetScreen();
          },
          child: Text("forgot_password".tr()),
        ),
      ],
    );
  }
}

class _ErrorText extends StatelessWidget {
  const _ErrorText({
    required String errorMessage,
  }) : _errorMessage = errorMessage;

  final String _errorMessage;

  @override
  Widget build(BuildContext context) {
    if (_errorMessage == "") {
      return const SizedBox(
        height: 16,
      );
    } else {
      return Column(
        children: [
          const SizedBox(height: 2),
          ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  _errorMessage,
                  style: const TextStyle(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}

class _GoToSignUp extends StatelessWidget {
  const _GoToSignUp();

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          if (context.read<AuthCubit>().state == AuthState.loading) return;
          context.read<AuthCubit>().goToSignUpScreen();
        },
        child: Text("register_now".tr()));
  }
}

class _SignInWithGoogleButton extends StatelessWidget {
  const _SignInWithGoogleButton();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(MdiIcons.google),
      onPressed: () {
        if (context.read<AuthCubit>().state == AuthState.loading) return;
        context.read<AuthCubit>().signInWithGoogle();
      },
    );
  }
}

class _SignInButton extends StatelessWidget {
  const _SignInButton({
    required TextEditingController emailController,
    required TextEditingController passwordController,
  })  : _emailController = emailController,
        _passwordController = passwordController;

  final TextEditingController _emailController;
  final TextEditingController _passwordController;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<AuthCubit>().state == AuthState.loading) return;
              FocusManager.instance.primaryFocus?.unfocus();
              context.read<AuthCubit>().signIn(email: _emailController.text, password: _passwordController.text);
              // context.read<AuthCubit>().signIn(email: "vobah54800@bixolabs.com", password: "12345zxcvb");
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("log_in".tr()),
          ),
        ),
      ],
    );
  }
}

class _PasswordTextField extends StatelessWidget {
  const _PasswordTextField({
    required TextEditingController passwordController,
  }) : _passwordController = passwordController;

  final TextEditingController _passwordController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _passwordController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "password".tr(),
      ),
      obscureText: true,
    );
  }
}

class _EmailTextField extends StatelessWidget {
  const _EmailTextField({
    required TextEditingController emailController,
  }) : _emailController = emailController;

  final TextEditingController _emailController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _emailController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "email".tr(),
      ),
    );
  }
}
