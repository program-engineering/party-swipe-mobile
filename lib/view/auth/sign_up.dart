import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../domain/auth/auth_repository.dart';
import 'auth_cubit.dart';
import 'auth_state.dart';
import 'sign_in.dart';
import 'email_verification.dart';
import '../home/home.dart';
import '../widgets/logo_image.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final _usernameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _repeatPasswordController = TextEditingController();
  var _exceptionMessage = "";

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthCubit(AuthRepository(FirebaseAuth.instance), AuthState.signUpScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: BlocListener<AuthCubit, AuthState>(
            listener: (context, state) {
              if (state == AuthState.success) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
              } else if (state == AuthState.signInScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const SignInPage()));
              } else if (state == AuthState.emailVerificationScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const EmailVerificationPage()));
              } else if (state.isException) {
                setState(() {
                  _exceptionMessage = state.message.tr();
                });
              }
              if (!state.isException) {
                _exceptionMessage = "";
              }
            },
            child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: SizedBox(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                    child: Column(
                      children: [
                        const Spacer(),
                        const LogoImage(),
                        const Spacer(),
                        const SizedBox(height: 16),
                        _UsernameTextField(usernameController: _usernameController),
                        const SizedBox(height: 16),
                        _EmailTextField(emailController: _emailController),
                        const SizedBox(height: 16),
                        _PasswordTextField(passwordController: _passwordController),
                        const SizedBox(height: 16),
                        _RepeatPasswordTextField(repeatPasswordController: _repeatPasswordController),
                        _ErrorText(errorMessage: _exceptionMessage),
                        _SignUpButton(
                          usernameController: _usernameController,
                          emailController: _emailController,
                          passwordController: _passwordController,
                          repeatPasswordController: _repeatPasswordController,
                        ),
                        const Spacer(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("have_account".tr()),
                            const _GoToSignIn(),
                          ],
                        ),
                      ],
                    ),
                  ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _ErrorText extends StatelessWidget {
  const _ErrorText({
    required String errorMessage,
  }) : _errorMessage = errorMessage;

  final String _errorMessage;

  @override
  Widget build(BuildContext context) {
    if (_errorMessage == "") {
      return const SizedBox(
        height: 16,
      );
    } else {
      return Column(
        children: [
          const SizedBox(height: 2),
          ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  _errorMessage,
                  style: const TextStyle(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}

class _GoToSignIn extends StatelessWidget {
  const _GoToSignIn();

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: () {
          if (context.read<AuthCubit>().state == AuthState.loading) return;
          context.read<AuthCubit>().goToSignInScreen();
        },
        child: Text("log_in_now".tr()));
  }
}

class _SignUpButton extends StatelessWidget {
  const _SignUpButton({
    required TextEditingController usernameController,
    required TextEditingController emailController,
    required TextEditingController passwordController,
    required TextEditingController repeatPasswordController,
  })  : _usernameController = usernameController,
        _emailController = emailController,
        _passwordController = passwordController,
        _repeatPasswordController = repeatPasswordController;

  final TextEditingController _usernameController;
  final TextEditingController _emailController;
  final TextEditingController _passwordController;
  final TextEditingController _repeatPasswordController;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<AuthCubit>().state == AuthState.loading) return;
              FocusManager.instance.primaryFocus?.unfocus();
              context.read<AuthCubit>().signUp(
                  username: _usernameController.text,
                  email: _emailController.text,
                  password: _passwordController.text,
                  repeatedPassword: _repeatPasswordController.text);
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("register".tr()),
          ),
        ),
      ],
    );
  }
}

class _UsernameTextField extends StatelessWidget {
  const _UsernameTextField({
    required TextEditingController usernameController,
  }) : _usernameController = usernameController;

  final TextEditingController _usernameController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _usernameController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "username".tr(),
      ),
    );
  }
}

class _PasswordTextField extends StatelessWidget {
  const _PasswordTextField({
    required TextEditingController passwordController,
  }) : _passwordController = passwordController;

  final TextEditingController _passwordController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _passwordController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "password".tr(),
      ),
      obscureText: true,
    );
  }
}

class _RepeatPasswordTextField extends StatelessWidget {
  const _RepeatPasswordTextField({
    required TextEditingController repeatPasswordController,
  }) : _repeatPasswordController = repeatPasswordController;

  final TextEditingController _repeatPasswordController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _repeatPasswordController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "rep_password".tr(),
      ),
      obscureText: true,
    );
  }
}

class _EmailTextField extends StatelessWidget {
  const _EmailTextField({
    required TextEditingController emailController,
  }) : _emailController = emailController;

  final TextEditingController _emailController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _emailController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "email".tr(),
      ),
    );
  }
}
