import 'package:flutter_bloc/flutter_bloc.dart';
import '../../domain/auth/auth_repository.dart';
import 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository authRepository;

  AuthCubit(this.authRepository, AuthState state) : super(state);

  void signIn({
    required String email,
    required String password,
  }) async {
    emit(AuthState.loading);
    final response = await authRepository.signIn(email: email, password: password);
    final isVerified = await authRepository.isEmailVerified();
    emit(
      response.fold(
        (authException) => exceptionToState(authException),
        (_) => isVerified.fold(
          (authException) => exceptionToState(authException),
          (b) => b ? AuthState.success : AuthState.emailVerificationScreen,
        ),
      ),
    );
  }

  void signUp(
      {required String username,
      required String email,
      required String password,
      required String repeatedPassword}) async {
    emit(AuthState.loading);
    final response = await authRepository.signUp(
        username: username, email: email, password: password, repeatedPassword: repeatedPassword);
    emit(
      response.fold(
        (authException) {
          return exceptionToState(authException);
        },
        (_) => AuthState.emailVerificationScreen,
      ),
    );
  }

  void resetPassword({required String email}) async {
    emit(AuthState.loading);
    var response = await authRepository.resetPassword(email: email);
    emit(response.fold((authException) {
      return exceptionToState(authException);
    }, (_) => AuthState.passwordResetEmailSent));
  }

  void signInWithGoogle() async {
    emit(AuthState.loading);
    final response = await authRepository.signInWithGoogle();
    emit(
      response.fold(
        (authException) {
          return exceptionToState(authException);
        },
        (_) => AuthState.success,
      ),
    );
  }

  void signOut() async {
    emit(AuthState.loading);
    final response = await authRepository.signOut();
    emit(
      response.fold(
        (authException) {
          return exceptionToState(authException);
        },
        (_) => AuthState.signInScreen,
      ),
    );
  }

  void checkEmailVerified() async {
    final isVerified = await authRepository.isEmailVerified();
    emit(
      isVerified.fold(
        (authException) => exceptionToState(authException),
        (b) => b ? AuthState.success : AuthState.emailVerificationScreen,
      ),
    );
  }

  void sendEmailVerification() async {
    await authRepository.sendEmailVerification();
  }

  void goToSignInScreen() {
    emit(AuthState.signInScreen);
  }

  void goToSignUpScreen() {
    emit(AuthState.signUpScreen);
  }

  void goToEmailVerificationScreen() {
    emit(AuthState.emailVerificationScreen);
  }

  void goToPasswordResetScreen() {
    emit(AuthState.passwordResetScreen);
  }

  void goToHomeScreen() {
    emit(AuthState.success);
  }
}
