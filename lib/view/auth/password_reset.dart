import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../domain/auth/auth_repository.dart';
import 'auth_cubit.dart';
import 'auth_state.dart';
import 'sign_in.dart';
import '../widgets/logo_image.dart';

class PasswordResetPage extends StatefulWidget {
  const PasswordResetPage({super.key});

  @override
  State<PasswordResetPage> createState() => _PasswordResetPageState();
}

class _PasswordResetPageState extends State<PasswordResetPage> {
  final _emailController = TextEditingController();
  var _exceptionMessage = "";
  bool _isEmailSent = false;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AuthCubit(AuthRepository(FirebaseAuth.instance), AuthState.passwordResetScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: BlocListener<AuthCubit, AuthState>(
            listener: (context, state) {
              if (state == AuthState.signInScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const SignInPage()));
              } else if (state == AuthState.passwordResetEmailSent) {
                setState(() {
                  _isEmailSent = true;
                });
              } else if (state.isException) {
                setState(() {
                  _exceptionMessage = state.message.tr();
                });
              }
              if (!state.isException) {
                _exceptionMessage = "";
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                  child: Column(
                    children: [
                      const LogoImage(),
                      const SizedBox(height: 16),
                      _EmailTextField(emailController: _emailController),
                      const SizedBox(height: 16),
                      _ResetPasswordButton(emailController: _emailController),
                      _ErrorText(errorMessage: _exceptionMessage),
                      _NotificationText(isEmailSent: _isEmailSent),
                      const Spacer(),
                      const _BackToSignInButton(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _NotificationText extends StatelessWidget {
  const _NotificationText({
    required bool isEmailSent,
  }) : _isEmailSent = isEmailSent;

  final bool _isEmailSent;

  @override
  Widget build(BuildContext context) {
    if (!_isEmailSent) {
      return const SizedBox(
        height: 16,
      );
    } else {
      return Column(
        children: [
          const SizedBox(height: 2),
          ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("email_sent".tr()),
              ],
            ),
          ),
        ],
      );
    }
  }
}

class _BackToSignInButton extends StatelessWidget {
  const _BackToSignInButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<AuthCubit>().state == AuthState.loading) return;
              context.read<AuthCubit>().goToSignInScreen();
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("back_to_sign_in".tr()),
          ),
        ),
      ],
    );
  }
}

class _ResetPasswordButton extends StatelessWidget {
  const _ResetPasswordButton({
    required TextEditingController emailController,
  }) : _emailController = emailController;

  final TextEditingController _emailController;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<AuthCubit>().state == AuthState.loading) return;
              FocusManager.instance.primaryFocus?.unfocus();
              context.read<AuthCubit>().resetPassword(email: _emailController.text);
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("reset_email".tr()),
          ),
        ),
      ],
    );
  }
}

class _EmailTextField extends StatelessWidget {
  const _EmailTextField({
    required TextEditingController emailController,
  }) : _emailController = emailController;

  final TextEditingController _emailController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _emailController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "email".tr(),
      ),
    );
  }
}

class _ErrorText extends StatelessWidget {
  const _ErrorText({
    required String errorMessage,
  }) : _errorMessage = errorMessage;

  final String _errorMessage;

  @override
  Widget build(BuildContext context) {
    if (_errorMessage == "") {
      return const SizedBox(
        height: 16,
      );
    } else {
      return Column(
        children: [
          const SizedBox(height: 2),
          ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 14),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  _errorMessage,
                  style: const TextStyle(
                    color: Colors.red,
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}
