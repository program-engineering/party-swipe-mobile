import '../../../domain/auth/exception.dart';

enum AuthState {
  signInScreen(""),
  signUpScreen(""),
  emailVerificationScreen(""),
  passwordResetScreen(""),
  passwordResetEmailSent(""),
  success(""),
  loading(""),
  emailAlreadyInUseException("email-already-in-use"),
  invalidEmailException("invalid-email"),
  weakPasswordException("weak-password"),
  userDisabledException("user-disabled"),
  tooManyRequestsException("too-many-requests"),
  invalidLoginCredentialsException("INVALID_LOGIN_CREDENTIALS"),
  wrongPasswordException("wrong-password"),
  userNotFoundException("user-not-found"),
  expiredActionCode("expired-action-code"),
  invalidActionCode("invalid-action-code"),
  operationNotAllowed("operation-not-allowed"),
  networkRequestFailed("network-request-failed"),
  passwordsDoNotMatchException("passwords_do_not_match"),
  unknownException("unknown");

  const AuthState(this.message);

  final String message; // contain info about the state or error code in case state is error

  bool get isException => exceptionFromCode(message) != AuthException.unknown || this == AuthState.unknownException;
}

AuthState exceptionToState(AuthException authException) {
  return switch (authException) {
    AuthException.emailAlreadyInUse => AuthState.emailAlreadyInUseException,
    AuthException.invalidEmail => AuthState.invalidEmailException,
    AuthException.weakPassword => AuthState.weakPasswordException,
    AuthException.userDisabled => AuthState.userDisabledException,
    AuthException.tooManyRequests => AuthState.tooManyRequestsException,
    AuthException.invalidLoginCredentials => AuthState.invalidLoginCredentialsException,
    AuthException.wrongPassword => AuthState.wrongPasswordException,
    AuthException.userNotFound => AuthState.userNotFoundException,
    AuthException.expiredActionCode => AuthState.expiredActionCode,
    AuthException.invalidActionCode => AuthState.invalidActionCode,
    AuthException.operationNotAllowed => AuthState.operationNotAllowed,
    AuthException.networkRequestFailed => AuthState.networkRequestFailed,
    AuthException.passwordsDoNotMatch => AuthState.passwordsDoNotMatchException,
    AuthException.unknown => AuthState.unknownException,
  };
}
