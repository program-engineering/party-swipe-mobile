import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../domain/auth/auth_repository.dart';
import 'auth_cubit.dart';
import 'auth_state.dart';
import 'sign_in.dart';
import '../home/home.dart';
import '../widgets/logo_image.dart';

class EmailVerificationPage extends StatefulWidget {
  const EmailVerificationPage({super.key});

  @override
  State<EmailVerificationPage> createState() => _EmailVerificationPageState();
}

class _EmailVerificationPageState extends State<EmailVerificationPage> {
  Timer? timer;

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        final cubit = AuthCubit(AuthRepository(FirebaseAuth.instance), AuthState.emailVerificationScreen);
        cubit.sendEmailVerification();
        timer = Timer.periodic(const Duration(seconds: 3), (_) => cubit.checkEmailVerified());
        return cubit;
      },
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: BlocListener<AuthCubit, AuthState>(
            listener: (context, state) {
              if (state == AuthState.success) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
              } else if (state == AuthState.signInScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const SignInPage()));
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                  child: Column(
                    children: [
                      const LogoImage(),
                      const SizedBox(height: 16),
                      Text(
                        "verify_email".tr(),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 16),
                      const _ResendEmailButton(),
                      const Spacer(),
                      const _BackToSignInButton(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _BackToSignInButton extends StatelessWidget {
  const _BackToSignInButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<AuthCubit>().state == AuthState.loading) return;
              context.read<AuthCubit>().goToSignInScreen();
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("back_to_sign_in".tr()),
          ),
        ),
      ],
    );
  }
}

class _ResendEmailButton extends StatelessWidget {
  const _ResendEmailButton();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ElevatedButton(
            onPressed: () {
              if (context.read<AuthCubit>().state == AuthState.loading) return;
              context.read<AuthCubit>().sendEmailVerification();
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Theme.of(context).colorScheme.onPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            child: Text("send_verification".tr()),
          ),
        ),
      ],
    );
  }
}
