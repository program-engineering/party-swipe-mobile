import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import '../navigation/navigation_bar.dart';
import '../../data/mock_events/mock_events.dart';
import '../home/home.dart';
import '../widgets/event_list/event_list.dart';
import 'all_events_cubit.dart';
import 'all_events_state.dart';

class AllEventsPage extends StatefulWidget {
  const AllEventsPage({super.key});

  @override
  State<AllEventsPage> createState() => _AllEventsPageState();
}

class _AllEventsPageState extends State<AllEventsPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AllEventsCubit(AllEventsState.allEventsScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          bottomNavigationBar: SizedBox(
            height: bottomNavigationBarHeight,
            child: CustomBottomNavigationBar(context, 2),
          ),
          body: BlocListener<AllEventsCubit, AllEventsState>(
            listener: (context, state) {
              if (state == AllEventsState.homeScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - bottomNavigationBarHeight,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(16, 32, 16, 0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          const _HomeButton(),
                          Expanded(
                              child: Text(
                            "all_events".tr(),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                                  color: Theme.of(context).colorScheme.primary,
                                ),
                          )),
                        ],
                      ),
                      const SizedBox(height: 4),
                      const Flexible(
                        child: EventList(
                          getEventList: getMockAllEventList,
                          hasLikes: false,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// class _SearchBar extends StatelessWidget {
//   const _SearchBar();
//
//   @override
//   Widget build(BuildContext context) {
//     return SearchAnchor(builder: (BuildContext context, SearchController controller) {
//       return SearchBar(
//         padding: const MaterialStatePropertyAll<EdgeInsets>(EdgeInsets.symmetric(horizontal: 16)),
//         backgroundColor: MaterialStatePropertyAll<Color>(Theme.of(context).colorScheme.secondaryContainer),
//         surfaceTintColor: const MaterialStatePropertyAll<Color>(Colors.transparent),
//         shadowColor: const MaterialStatePropertyAll<Color>(Colors.transparent),
//         controller: controller,
//         onTap: () {
//           controller.openView();
//         },
//         onChanged: (_) {
//           controller.openView();
//         },
//         hintText: "search".tr(),
//         leading: const Icon(Icons.search),
//         trailing: const [Icon(Icons.menu)],
//       );
//     }, suggestionsBuilder: (BuildContext context, SearchController controller) {
//       return List<ListTile>.empty();
//     });
//   }
// }

class _HomeButton extends StatelessWidget {
  const _HomeButton();

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        if (context.read<AllEventsCubit>().state == AllEventsState.loading) return;
        context.read<AllEventsCubit>().goToHomeScreen();
      },
      icon: const Icon(Icons.keyboard_arrow_left_sharp),
    );
  }
}
