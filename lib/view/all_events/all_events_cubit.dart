import 'package:flutter_bloc/flutter_bloc.dart';
import 'all_events_state.dart';

class AllEventsCubit extends Cubit<AllEventsState> {
  AllEventsCubit(AllEventsState state) : super(state);

  void goToHomeScreen() {
    emit(AllEventsState.homeScreen);
  }

}
