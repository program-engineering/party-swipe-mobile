enum AllEventsState {
  homeScreen(""),
  allEventsScreen(""),
  loading(""),
  unknownException("unknown");

  const AllEventsState(this.message);

  final String message;
}
