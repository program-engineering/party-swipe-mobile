import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:party_swipe_mobile/domain/add_event/add_event_repository.dart';
import '../../data/all_categories/all_categories.dart';
import '../../domain/event/event.dart';
import '../my_events/my_events.dart';
import '../widgets/category_list.dart';
import 'add_event_state.dart';
import 'add_event_cubit.dart';

class AddEventScreen extends StatefulWidget {
  const AddEventScreen({super.key});

  @override
  State<AddEventScreen> createState() => _AddEventState();
}

class _AddEventState extends State<AddEventScreen> {
  final Event _currentEvent = emptyEvent();
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _dateController = TextEditingController();
  final _timeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AddEventCubit(AddEventRepository(), AddEventState.addEventScreen),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: BlocListener<AddEventCubit, AddEventState>(
            listener: (context, state) {
              if (state == AddEventState.myEventsScreen) {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => const MyEventsPage()));
              }
            },
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                  child: Column(
                    children: [
                      const Spacer(),
                      const _AddEventTitle(),
                      const SizedBox(height: 16),
                      _NameEvent(nameController: _nameController, currentEvent: _currentEvent),
                      const SizedBox(height: 16),
                      _DescriptionEvent(descriptionController: _descriptionController, currentEvent: _currentEvent),
                      const SizedBox(height: 16),
                      _DateEvent(dateController: _dateController, currentEvent: _currentEvent),
                      const SizedBox(height: 16),
                      _TimeEvent(timeTimeEventController: _timeController, currentEvent: _currentEvent),
                      const SizedBox(height: 16),
                      _CategoriesEvent(currentEvent: _currentEvent),
                      const SizedBox(height: 16),
                      _AddEvent(currentEvent: _currentEvent),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _NameEvent extends StatelessWidget {
  const _NameEvent({
    required TextEditingController nameController,
    required Event currentEvent,
  })  : _nameController = nameController,
        _currentEvent = currentEvent;

  final TextEditingController _nameController;
  final Event _currentEvent;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _nameController,
      onSubmitted: (text) => (_currentEvent.name = text),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "name_event".tr(),
      ),
    );
  }
}

class _DescriptionEvent extends StatelessWidget {
  const _DescriptionEvent({
    required TextEditingController descriptionController,
    required Event currentEvent,
  })  : _descriptionController = descriptionController,
        _currentEvent = currentEvent;

  final TextEditingController _descriptionController;
  final Event _currentEvent;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _descriptionController,
      onSubmitted: (text) => (_currentEvent.description = text),
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "description_event".tr(),
      ),
    );
  }
}

class _DateEvent extends StatelessWidget {
  const _DateEvent({
    required TextEditingController dateController,
    required Event currentEvent,
  })  : _dateController = dateController,
        _currentEvent = currentEvent;

  final TextEditingController _dateController;
  final Event _currentEvent;

  @override
  Widget build(BuildContext context) {
    final localizations = MaterialLocalizations.of(context);
    
    return TextField(
      controller: _dateController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "date_event".tr(),
        icon: const Icon(Icons.calendar_today),
      ),
      readOnly: true,
      onTap: () async {
        DateTime? pickedDate = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime(2000),
          lastDate: DateTime(2100),
        );

        if (pickedDate != null) {
          _dateController.text = localizations.formatFullDate(pickedDate);
          final currentTime = TimeOfDay.fromDateTime(_currentEvent.datetime);
          _currentEvent.datetime =
              DateTime(pickedDate.year, pickedDate.month, pickedDate.day, currentTime.hour, currentTime.minute);
        }
      },
    );
  }
}

class _TimeEvent extends StatelessWidget {
  const _TimeEvent({
    required TextEditingController timeTimeEventController,
    required Event currentEvent,
  })  : _timeController = timeTimeEventController,
        _currentEvent = currentEvent;

  final TextEditingController _timeController;
  final Event _currentEvent;

  @override
  Widget build(BuildContext context) {
    final localizations = MaterialLocalizations.of(context);
    
    return TextField(
      controller: _timeController,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        labelText: "time_event".tr(),
        icon: const Icon(Icons.access_time),
      ),
      readOnly: true,
      onTap: () async {
        final TimeOfDay? pickedTime = await showTimePicker(
          context: context,
          initialTime: TimeOfDay.now(),
        );
        if (pickedTime != null) {
          _timeController.text = localizations.formatTimeOfDay(pickedTime);
          final currentDate = _currentEvent.datetime;
          _currentEvent.datetime =
              DateTime(currentDate.year, currentDate.month, currentDate.day, pickedTime.hour, pickedTime.minute);
        }
      },
    );
  }
}

class _AddEventTitle extends StatelessWidget {
  const _AddEventTitle();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          "New Event",
          style: TextStyle(
            color: Theme.of(context).primaryColor,
            fontWeight: FontWeight.bold,
            fontSize: 25,
          ),
        ),
        const Text(
          "Please specify the parameters of your event",
          style: TextStyle(
            color: Colors.black45,
          ),
        )
      ],
    );
  }
}

class _CategoriesEvent extends StatefulWidget {
  const _CategoriesEvent({
    required Event currentEvent,
  }) : _currentUser = currentEvent;

  final Event _currentUser;

  @override
  State<_CategoriesEvent> createState() => _CategoriesEventState();
}

class _CategoriesEventState extends State<_CategoriesEvent> {
  late TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Autocomplete<String>(
          onSelected: (text) {
            setState(() {
              if (!widget._currentUser.categories.contains(text)) {
                widget._currentUser.categories.add(text);
              } else {
                widget._currentUser.categories.remove(text);
              }
            });
            textEditingController.text = "";
            // FocusManager.instance.primaryFocus?.unfocus();
          },
          optionsBuilder: (TextEditingValue textEditingValue) {
            return allCategories
                .where((String option) => option.toLowerCase().contains(textEditingValue.text.toLowerCase()));
          },
          optionsViewBuilder: (context, onSelected, options) {
            return Align(
              alignment: Alignment.topLeft,
              child: Material(
                borderRadius: BorderRadius.circular(16),
                color: Theme.of(context).colorScheme.secondaryContainer,
                child: SizedBox(
                  height: 200.0,
                  width: MediaQuery.of(context).size.width - 32,
                  child: ListView.separated(
                    padding: EdgeInsets.zero,
                    itemCount: options.length,
                    separatorBuilder: (context, index) {
                      return Container(height: 1, color: Colors.black);
                    },
                    itemBuilder: (BuildContext context, int index) {
                      final option = options.elementAt(index);
                      return InkWell(
                        onTap: () {
                          onSelected(option);
                        },
                        child: Container(
                          padding: const EdgeInsets.all(16),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 16,
                                height: 16,
                                child: Checkbox(
                                  value: widget._currentUser.categories.contains(option),
                                  onChanged: (value) => onSelected(option),
                                ),
                              ),
                              const SizedBox(width: 16),
                              Text(
                                option,
                                style: TextStyle(color: Theme.of(context).colorScheme.onSecondaryContainer),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
            );
          },
          fieldViewBuilder: (BuildContext context, TextEditingController fieldTextEditingController,
              FocusNode fieldFocusNode, VoidCallback onFieldSubmitted) {
            textEditingController = fieldTextEditingController;
            return TextField(
              controller: fieldTextEditingController,
              focusNode: fieldFocusNode,
              onSubmitted: (text) {
                fieldTextEditingController.text = "";
                FocusManager.instance.primaryFocus?.unfocus();
              },
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
                labelText: "categories_event".tr(),
              ),
            );
          },
        ),
        const SizedBox(height: 16),
        CategoryList(categories: widget._currentUser.categories),
      ],
    );
  }
}

class _AddEvent extends StatelessWidget {
  const _AddEvent({
    required Event currentEvent,
  }) : _currentEvent = currentEvent;

  final Event _currentEvent;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Align(
            alignment: FractionalOffset.topRight,
            child: ElevatedButton(
              onPressed: () {
                if (context.read<AddEventCubit>().state == AddEventState.loading) return;
                context.read<AddEventCubit>().newEvent(event: _currentEvent);
                context.read<AddEventCubit>().goToMyEventsScreen();
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: Theme.of(context).primaryColor,
                foregroundColor: Theme.of(context).colorScheme.onPrimary,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              child: const Text("Add Event +"),
            ),
          ),
        ),
      ],
    );
  }
}
