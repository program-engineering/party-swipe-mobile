enum AddEventState {
  addEventScreen(""),
  myEventsScreen(""),
  loading(""),
  unknownException("unknown");

  const AddEventState(this.message);

  final String message;
}
