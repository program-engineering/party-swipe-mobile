import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:party_swipe_mobile/domain/add_event/add_event_repository.dart';
import '../../domain/event/event.dart';
import 'add_event_state.dart';

class AddEventCubit extends Cubit<AddEventState> {
  final AddEventRepository addEventRepository;

  AddEventCubit(this.addEventRepository, AddEventState state) : super(state);

  void newEvent({required Event event}) async {
    emit(AddEventState.loading);
    final response = addEventRepository.addEvent(event: event);
    emit(AddEventState.myEventsScreen);
  }

  void goToMyEventsScreen() {
    emit(AddEventState.myEventsScreen);
  }
}
