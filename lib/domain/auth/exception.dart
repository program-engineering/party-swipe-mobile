enum AuthException {
  emailAlreadyInUse("email-already-in-use"),
  invalidEmail("invalid-email"),
  weakPassword("weak-password"),
  userDisabled("user-disabled"),
  tooManyRequests("too-many-requests"),
  invalidLoginCredentials("INVALID_LOGIN_CREDENTIALS"), // wrong-password or user-not-found
  wrongPassword("wrong-password"),
  userNotFound("user-not-found"),
  expiredActionCode("expired-action-code"),
  invalidActionCode("invalid-action-code"),
  operationNotAllowed("operation-not-allowed"),
  networkRequestFailed("network-request-failed"),
  passwordsDoNotMatch("passwords_do_not_match"),
  unknown("unknown");

  const AuthException(this.message);

  final String message;
}

AuthException exceptionFromCode(String code) {
  return switch (code) {
    "email-already-in-use" => AuthException.emailAlreadyInUse,
    "invalid-email" => AuthException.invalidEmail,
    "weak-password" => AuthException.weakPassword,
    "user-disabled" => AuthException.userDisabled,
    "too-many-requests" => AuthException.tooManyRequests,
    "INVALID_LOGIN_CREDENTIALS" => AuthException.invalidLoginCredentials,
    "wrong-password" => AuthException.wrongPassword,
    "user-not-found" => AuthException.userNotFound,
    "expired-action-code" => AuthException.expiredActionCode,
    "invalid-action-code" => AuthException.invalidActionCode,
    "operation-not-allowed" => AuthException.operationNotAllowed,
    "network-request-failed" => AuthException.networkRequestFailed,
    "passwords_do_not_match" => AuthException.passwordsDoNotMatch,
    _ => AuthException.unknown,
  };
}
