import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:party_swipe_mobile/domain/auth/exception.dart';

class AuthRepository {
  final FirebaseAuth firebaseAuth;

  AuthRepository(this.firebaseAuth);

  Future<Either<AuthException, void>> signIn({required String email, required String password}) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      // return credential.user?.uid;
      return const Right(null);
    } on FirebaseAuthException catch (e) {
      return Left(exceptionFromCode(e.code));
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  Future<Either<AuthException, void>> signUp(
      {required String username,
        required String email,
        required String password,
        required String repeatedPassword}) async {
    try {
      if (password != repeatedPassword) {
        return const Left(AuthException.passwordsDoNotMatch);
      }
      final userCredential = await firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      final user = userCredential.user;
      // assert (user == firebaseAuth.currentUser);
      if (user == null) {
        return const Left(AuthException.unknown);
      }
      firebaseAuth.currentUser!.updateDisplayName(username);
      return const Right(null);
    } on FirebaseAuthException catch (e) {
      return Left(exceptionFromCode(e.code));
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  Future<Either<AuthException, void>> signInWithGoogle() async {
    try {
      if (kIsWeb) {
        GoogleAuthProvider googleProvider = GoogleAuthProvider();

        googleProvider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

        await firebaseAuth.signInWithPopup(googleProvider);
      } else {
        final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
        final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;
        final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth?.accessToken,
          idToken: googleAuth?.idToken,
        );
        await firebaseAuth.signInWithCredential(credential);
      }
      // return credential.user?.uid;
      return const Right(null);
    } on FirebaseAuthException catch (e) {
      return Left(exceptionFromCode(e.code));
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  Future<Either<AuthException, void>> resetPassword({
    required String email,
  }) async {
    try {
      await firebaseAuth.sendPasswordResetEmail(email: email);
      return const Right(null);
    } on FirebaseAuthException catch (e) {
      return Left(exceptionFromCode(e.code));
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  Future<Either<AuthException, void>> signOut() async {
    try {
      await firebaseAuth.signOut();
      return const Right(null);
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  Future<Either<AuthException, bool>> isEmailVerified() async {
    try {
      await firebaseAuth.currentUser?.reload();
      return Right(firebaseAuth.currentUser?.emailVerified ?? false);
    } on FirebaseAuthException catch (e) {
      return Left(exceptionFromCode(e.code));
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  Future<Either<AuthException, void>> sendEmailVerification() async {
    try {
      await firebaseAuth.currentUser!.reload();
      firebaseAuth.currentUser!.sendEmailVerification();
      return const Right(null);
    } on FirebaseAuthException catch (e) {
      return Left(exceptionFromCode(e.code));
    } catch (_) {
      return const Left(AuthException.unknown);
    }
  }

  String? getUserID() => firebaseAuth.currentUser?.uid;

  String? getUsername() => firebaseAuth.currentUser?.displayName;

  String? getEmail() => firebaseAuth.currentUser?.email;
}
