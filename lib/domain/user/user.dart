import '../../data/defaultPictures/defaultPictureUrls.dart';

class User {
  String id;
  String bio;
  String pictureUrl;
  List<String> categories;

  User(
    this.id,
    this.bio,
    this.pictureUrl,
    this.categories,
  );
}

User emptyUser() => User("", "", defaultProfilePictureUrl, List.empty());
