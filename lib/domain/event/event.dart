import '../../data/defaultPictures/defaultPictureUrls.dart';

class Event {
  String id;
  String name;
  String pictureUrl;
  String location;
  DateTime datetime;
  String description;
  List<String> categories;
  int likes;

  Event(
    this.id,
    this.name,
    this.pictureUrl,
    this.location,
    this.datetime,
    this.description,
    this.categories,
    this.likes,
  );
}

Event emptyEvent() => Event("", "", defaultEventPictureUrl, "", DateTime.fromMillisecondsSinceEpoch(0), "", [], 0);


