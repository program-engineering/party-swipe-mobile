import 'package:dartz/dartz.dart';

import '../event/event.dart';
import 'exception.dart';

class AddEventRepository {
  Left<AddEventException, dynamic> addEvent({required Event event}) {
    try {
      // await call();
      // POST -> /events/{userId}
      // return const Right(null);
      return const Left(AddEventException.unknown);
    } catch (_) {
      return const Left(AddEventException.unknown);
    }
  }
}
