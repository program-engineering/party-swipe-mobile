enum AddEventException {
  unknown("unknown");

  const AddEventException(this.message);

  final String message;
}